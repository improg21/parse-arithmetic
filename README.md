# Translate a grammar into a Haskell parser

Here is my experience with translating a grammar into a Haskell
parser. A grammar that is free of left recursion and
satisfies the two LL(1) conditions is the prerequisite for what
follows.

The following approach is just my personal approach. Please read it as a suggestion only. Other approaches are possible.

Consider the rule `a ::= b c | d e`. Assume, all lower case letters
are non-terminal symbols, `first(b) == {B}` and `first(d) ==
{D}`. First, I translated the rule as follows.

```haskell
a :: [Token] -> (Maybe AstA, [Token])
a tokensRest0@(B : _) =
  case b tokensRest0 of
    (Nothing, tokensRest1) -> (Nothing, tokensRest1)
    (Just astB, tokensRest1) ->
      case c tokensRest1 of
        (Nothing, tokensRest2) -> (Nothing, tokensRest2)
        (Just astC, tokensRest2) -> (Just $ f astB astC, tokensRest2)
a tokensRest0@(D : _) =
  case d tokensRest0 of
    (Nothing, tokensRest1) -> (Nothing, tokensRest1)
    (Just astD, tokensRest1) ->
      case e tokensRest1 of
        (Nothing, tokensRest2) -> (Nothing, tokensRest2)
        (Just astE, tokensRest2) -> (Just $ g astD astE, tokensRest2)
a tokensRest0 = (Nothing, tokensRest0)

f :: AstB -> AstC -> AstA
g :: AstD -> AstE -> AstA
b :: [Token] -> (Maybe AstB, [Token])
c :: [Token] -> (Maybe AstC, [Token])
d :: [Token] -> (Maybe AstD, [Token])
e :: [Token] -> (Maybe AstE, [Token])
```

Shortcuts might be possible in practice depending on the concrete rule. Find a runnable practical example at [src/ArithmeticParserVerbose.hs](src/ArithmeticParserVerbose.hs).

The implementation we have got so far works sufficiently well already. So everything below is completely optional.

I then refactored the implementation. The `Parser` type became an instance of `Functor`, `Applicative`, and `Monad`. This results in the following implementation.

```haskell
a :: Parser Token AstA
a =
  fromAlternatives alternatives
  where
    alternatives (Token B) = f <$> b <*> c
    alternatives (Token D) = g <$> d <*> e
    alternatives _ = fail "expected an a"

newtype Parser token a = Parser ([token] -> (Either String a, [token]))
f :: AstB -> AstC -> AstA
g :: AstD -> AstE -> AstA
b :: Parser Token AstB
c :: Parser Token AstC
d :: Parser Token AstD
e :: Parser Token AstE
```

Notice that we used the abstraction to manage some rudimentary error information too. So `Maybe a` turned into `Either String a`.

The `Functor`, `Applicative`, and `Monad` interfaces provide many usefule combinators in addition to `<$>` and `<*>`. Find a runnable practical example at [src/ArithmeticParserAbstract.hs](src/ArithmeticParserAbstract.hs). You can find the instance implementations there too.

## Run

```
stack run
```
