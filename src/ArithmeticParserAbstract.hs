module ArithmeticParserAbstract where

import Text.Pretty.Simple (pPrint)
import Relude

data Token =
  Number Text |
  Plus |
  RoundBracketOpen |
  RoundBracketClose
  deriving (Show, Eq)

data Expression =
  Literal Text |
  Addition Expression Expression
  deriving (Show)

{-
Backus–Naur form

expression ::=
  Number expressionMore |
  RoundBracketOpen expression RoundBracketClose expressionMore

plusExpression ::= Plus expression | \epsilon

-}

expression :: Parser Token Expression
expression =
  fromAlternatives alternatives
  where
    alternatives (Token (Number _)) =
      addExpression <$> (Literal <$> number) <*> plusExpression
    alternatives (Token RoundBracketOpen) =
      addExpression
        <$> (single RoundBracketOpen *> expression <* single RoundBracketClose)
        <*> plusExpression
    alternatives _ = fail "expected an expression"

data PlusExpression = NoPlus | JustPlus Expression

addExpression :: Expression -> PlusExpression -> Expression
addExpression e NoPlus = e
addExpression e0 (JustPlus e1) = Addition e0 e1

plusExpression :: Parser Token PlusExpression
plusExpression =
  fromAlternatives alternatives
  where
    alternatives (Token Plus) = single Plus *> (JustPlus <$> expression)
    alternatives _ = pure NoPlus

number :: Parser Token Text
number =
  do
    token <- anySingle
    case token of
      Number text -> pure text
      _ -> fail "expected a literal"

test =
  runParser
    expression
    [
      Number "2",
      Plus,
      RoundBracketOpen,
      RoundBracketOpen,Number "3", RoundBracketClose,
      Plus,
      Number "5",
      RoundBracketClose
    ]

main :: IO ()
main = pPrint test

-- $> ArithmeticParserAbstract.main

-- begin of library

newtype Parser token a = Parser ([token] -> (Either String a, [token]))
runParser :: Parser token a -> [token] -> (Either String a, [token])
runParser (Parser function) = function

instance Functor (Parser token) where
  fmap f (Parser parser) =
    Parser
      (\tokensOld ->
        let (result, tokensNew) = parser tokensOld
        in (f <$> result, tokensNew)
      )

instance Applicative (Parser token) where
  pure result = Parser (\tokens -> (Right result, tokens))
  (Parser parser1) <*> (Parser parser2) =
    Parser
      (\tokensOld1 ->
        let
          (result1, tokensOld2) = parser1 tokensOld1
          (result2, tokensNew) = parser2 tokensOld2
        in case result1 of
          Left message -> (Left message, tokensOld2)
          Right f -> (f <$> result2, tokensNew)
      )

instance Monad (Parser token) where
  return = pure
  (Parser parser1) >>= f =
    Parser
      (\tokensOld1 ->
         let
           (result1, tokensOld2) = parser1 tokensOld1
         in case result1 of
           Left message -> (Left message, tokensOld2)
           Right parseResult ->
             let (Parser parser2) = f parseResult
             in parser2 tokensOld2
      )

instance MonadFail (Parser token) where
  fail message = Parser (\tokens -> (Left message, tokens))

single :: (Show token, Eq token) => token -> Parser token token
single token =
  Parser function
  where
    function tokensOld
      | tokenNext : tokensRest <- tokensOld, tokenNext == token =
        (Right tokenNext, tokensRest)
      | otherwise = (Left $ "expected " <> show token, tokensOld)

satisfy :: (token -> Bool) -> Parser token token
satisfy predicate =
  Parser function
  where
    function tokensOld
      | tokenNext : tokensRest <- tokensOld, predicate tokenNext =
        (Right tokenNext, tokensRest)
      | otherwise = (Left "", tokensOld)

anySingle :: Parser token token
anySingle = satisfy (const True)

data TokenOrEndOfInput token = Token token | EndOfInput

fromAlternatives ::
  (TokenOrEndOfInput token -> Parser token result) -> Parser token result
fromAlternatives alternatives =
  Parser
    (\tokens -> runParser (alternatives $ nextToken tokens) tokens)
  where
    nextToken [] = EndOfInput
    nextToken (token : _) = Token token
