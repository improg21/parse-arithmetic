module Main (main) where

import ArithmeticParserVerbose qualified
import ArithmeticParserAbstract qualified

import Relude

main :: IO ()
main =
  putTextLn "ArithmeticParserVerbose:" *>
  ArithmeticParserVerbose.main *>
  putTextLn "ArithmeticParserAbstract:" *>
  ArithmeticParserAbstract.main
